/**
 * Created by Carlos Canela on 13/02/20.
 */
let heroHomeReelTimer = 0;
let pageFocus = true;
$(function () {
    initHeroReel();
});

function initHeroReel() {
    $(window).on('resize', function () {
        let na = 0;
        $("#aviso_hero .pager").empty();
        $("#aviso_hero .hero_card").each(function () {
            $(this).width($("#aviso_hero").outerWidth());
            na += $(this).outerWidth(true)
            $("#aviso_hero .pager").append($("<li>"))
        });
        $("#aviso_hero .hero_reel").width(na);
        $("#aviso_hero .pager li:first").click();
    }).resize();
    $("#aviso_hero .pager").on('click', 'li', function () {
        clearTimeout(heroHomeReelTimer);
        let dest = 0;
        dest = $(this).index() * $("#aviso_hero .hero_card:first").outerWidth(true);
        $("#aviso_hero .hero_reel").animate({
            left: -dest
        }, {
            duration: (pageFocus) ? 600 : 0,
            easing: 'swing'
        })
        $(this).siblings('li').removeClass('active')
        $(this).addClass('active')
        heroHomeReelTimer = setTimeout(function () {
            let sig = $("#aviso_hero .pager li.active").next();
            if (sig.length == 0) {
                //llegamos al final, devemos reiniciar
                sig = $("#aviso_hero .pager li:first");
            }
            sig.click();
        }, 6000)
    })
    $(window).on('blur', function () {
        pageFocus = false;
    })
    $(window).on('focus', function () {
        pageFocus = true;
    })//TODO si no funciona, animate stop
    $("#aviso_hero .pager li:first").click();
}
