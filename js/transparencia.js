/**
 * Created by Carlos Canela on 14/02/20.
 */
$(function () {
    loadPresupuesto();
    initOrdenaTabla();
})

function loadPresupuesto() {
    for (p = 0; p <= 50; p++) {
        let pr = $(".programa_row.d-none").clone();
        let bdg = (Math.random() * 300000000) + 1000000;
        let benf = Math.round((Math.random() * 23000000) + 5634);
        pr.find('.currency_presupuesto').text("$" + bdg.format(2)).data('cantidad', bdg);
        pr.find('.cantidad_beneficiarios').text(benf.format()).data('cantidad', benf);
        pr.removeClass('d-none')
        $("#prog_rows_container").append(pr);
    }
    //ordenar
    ordenar($(".programa_row:not('.d-none'):first"), 'desc', '.currency_presupuesto')
}

// let pre_ord=null;
function ordenar(row, by, clase) {
    let cant_act = parseFloat(row.find(clase).data('cantidad'));
    let cant_ant = parseFloat(row.prev('.programa_row').find(clase).data('cantidad'));
    let row_act = row;
    let row_ant = row.prev('.programa_row');
    if (row_ant.length == 0) {
        // console.log('CERO')
        ordenar(row_act.next('.programa_row'), by, clase);
    }
    let operacion = (by == "desc") ? "cant_act > cant_ant" : 'cant_act < cant_ant';
    if (eval(operacion)) {
        row_ant.before(row_act);
        // console.log('Actual',cant_act,'>',cant_ant);
        ordenar(row_act, by, clase)
    } else if (row_act.next('.programa_row').length > 0) {
        // console.log('Actual Menor o igual, Siguiente');
        ordenar(row_act.next('.programa_row'), by, clase)
    } else {
        // console.log('FIN')
        return true;
    }
}

function initOrdenaTabla() {
    $("#tabla_programas th[data-target]").click(function () {
        let orby = $(this).attr('data-sort');
        orby = (orby == undefined) ? 'asc' : orby;
        orby=(orby=='desc')?'asc':'desc';
        ordenar($(".programa_row:not('.d-none'):first"), orby, "."+$(this).attr('data-target'))
        $(this).attr('data-sort',orby);
        $(this).siblings('th').removeAttr('data-sort')
    })
}

/**
 * Number.prototype.format(n, x)
 *
 * @param integer n: length of decimal
 * @param integer x: length of sections
 */
Number.prototype.format = function (n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};
