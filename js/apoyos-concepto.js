/**
 * Created by Carlos Canela on 19/02/20.
 */
$(function () {
    //obtener ID de concepto
    let cid = parametroURL('cid');
    if (!cid) {
        alert('Se requiere un identificador de componente');
    } else {
        loadConcepto(cid);
        loadProductosRel(cid);
    }
})

function loadConcepto(cid) {
    $.ajax({
        url: 'https://suri-des.agricultura.gob.mx:8016/api/conceptos',
        context: {cid: cid},
        success: function (data) {
            for (d in data) {
                if (data[d].concepto_id == this.cid) {
                    $(".concepto-title h1").text(data[d].concepto_titulo);
                    $("#concepto-tab .concepto_beneficio_desc_gral p").html(data[d].concepto_beneficio_desc_gral.replace(/\\n/g, '<br/>'));
                    $("#concepto-tab .concepto_principal_beneficio").html(data[d].concepto_principal_beneficio.replace(/\\n/g, '<br/>'));
                    $("#concepto-tab .conceptofechaapertura").text(data[d].conceptofechaapertura);
                    $("#concepto-tab .conceptofechacierre").text(data[d].conceptofechacierre);
                    $("#concepto-tab .concepto_monto_max_gral p").html(data[d].concepto_monto_max_gral.replace(/\\n/g, '<br/>'));
                    $("#concepto-tab .concepto_notas p").html(data[d].concepto_notas.replace(/\\n/g, '<br/>'));
                    loadTiposConcepto(data[d].tipo_concepto_id);
                    loadComponente(data[d].componente_id);
                    loadElementosRel(data[d].concepto_id);
                    loadConsideraciones(data[d].concepto_id);
                    loadRequisitos(data[d].concepto_id);
                }
            }
        }
    })
}

function loadTiposConcepto(tid) {
    $.ajax({
        url: 'https://suri-des.agricultura.gob.mx:8016/api/tpo_concepto',
        context: {tid: tid},
        success: function (data) {
            for (d in data) {
                if (data[d].tipo_concepto_id == this.tid) {
                    $(".concepto-tipo p").text(data[d].tipo_concepto_nombre)
                }
            }
        }
    })
}

function loadComponente(cid) {
    $.ajax({
        url: 'https://suri-des.agricultura.gob.mx:8016/api/componentes',
        context: {cid: cid},
        success: function (data) {
            for (d in data) {
                if (data[d].componente_id == this.cid) {
                    $(".concepto-programa p").text(data[d].componente_nombre);
                    $("a.seccion-badge").attr('href', function () {
                        return $(this).attr('href') + data[d].componente_pdf
                    })
                }
            }
        }
    })
}


function loadElementosRel(cid) {
    $.ajax({
        url: 'https://suri-des.agricultura.gob.mx:8016/api/conceptos/rel/elementos',
        context: {cid: cid},
        success: function (data) {
            let concepto_elementos_rel = [];
            for (d in data) {
                if (this.cid == data[d].concepto_id) {
                    concepto_elementos_rel.push(data[d].elemento_id)
                }
            }
            loadElementosDetail(concepto_elementos_rel);
        }
    })
}

function loadElementosDetail(els) {
    $.ajax({
        url: 'https://suri-des.agricultura.gob.mx:8016/api/elementos',
        context: {els: els},
        success: function (data) {
            for (d in data) {
                if (this.els.indexOf(data[d].elemento_id) != -1) {
                    $("#concepto-elementos_list").append($("<li>", {html: data[d].elemento_nombre}))
                }
            }
        }
    })
}

function loadConsideraciones(cid) {
    $.ajax({
        url: 'https://suri-des.agricultura.gob.mx:8016/api/conceptos/rel/consideraciones',
        context: {cid: cid},
        success: function (data) {
            for (d in data) {
                if (data[d].concepto_id == this.cid) {
                    $("#concepto-consideraciones_list").append($("<li>", {html: data[d].consideracion}))
                }
            }
        }
    })
}

function loadRequisitos(cid) {
    $.ajax({
        url: 'https://suri-des.agricultura.gob.mx:8016/api/conceptos/rel/requisitos',
        context: {cid: cid},
        success: function (data) {
            for (d in data) {
                if (data[d].concepto_id == this.cid) {
                    $("#concepto-requisitos_list").append($("<li>", {html: data[d].requisito}))
                }
            }
        }
    })
}

function loadProductosRel(cid) {
    $.ajax({
        url: 'https://suri-des.agricultura.gob.mx:8016/api/conceptos/rel/productos/',
        context: {cid: cid},
        success: function (data) {
            let prods = [];
            for (d in data) {
                if (data[d].concepto_id == this.cid) {
                    prods.push(data[d].sector_producto_id)
                }
            }
            loadProductosDetail(prods);
        }
    })
}

function loadProductosDetail(prods) {
    $.ajax({
        url: 'https://suri-des.agricultura.gob.mx:8016/api/productos',
        context: {els: prods},
        success: function (data) {
            for (d in data) {
                if (this.els.indexOf(data[d].id) != -1) {
                    $("#productos_list-sector_" + data[d].id_sector.toString()).append($("<li>", {html: data[d].producto}))
                }
            }
            //productos cargados, revisamos sectores vacios para borrarlos
            $("ul[id^='productos_list-sector_']").each(function () {
                if ($(this).find('li').length == 0) {
                    $(this).parents('.sector-card').remove();
                }
            })
        }
    })
}

function parametroURL(_par) {
    var _p = null;
    if (location.search) location.search.substr(1).split("&").forEach(function (pllv) {
        var s = pllv.split("="), //separamos llave/valor
            ll = s[0],
            v = s[1] && decodeURIComponent(s[1]); //valor hacemos encode para prevenir url encode
        if (ll == _par) { //solo nos interesa si es el nombre del parametro a buscar
            if (_p == null) {
                _p = v; //si es nula, quiere decir que no tiene valor, solo textual
            } else if (Array.isArray(_p)) {
                _p.push(v); //si ya es arreglo, agregamos este valor
            } else {
                _p = [_p, v]; //si no es arreglo, lo convertimos y agregamos este valor
            }
        }
    });
    return _p;
}
