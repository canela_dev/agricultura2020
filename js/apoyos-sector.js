/**
 * Created by Carlos Canela on 11/02/20.
 */
let productos_filter = [];
$(function () {
    loadTiposConcepto();
    initProductFilters();
    initMobileTabs();
    initTabsGpos();
});

function loadTiposConcepto() {
    $.ajax({
        url: 'https://suri-des.agricultura.gob.mx:8016/api/gpo_conceptos',
        success: function (data) {
            $("#side_bar-tipoconcepto ul.conceptos-tabs").empty();
            $(data).each(function (i) {
                let gpo = $(this).get(0);
                $("#side_bar-tipoconcepto ul.conceptos-tabs").append($("<li>").append($("<a>", {href: '#' + gpo.grupo_conceptos_id, text: gpo.grupo_conceptos_nombre})))
            })
            $("#side_bar-tipoconcepto .conceptos-tabs li:first").click();
        }
    })
}

function initTabsGpos() {
    $("#side_bar-tipoconcepto .conceptos-tabs").on('click', 'li', function (ev) {
        ev.preventDefault();
        console.log($(this).find('a').attr('href').substr(1));
        $(this).addClass('active').siblings('li').removeClass('active')
        loadConceptos($(this).find('a').attr('href').substr(1))
    })
}

function loadConceptos(gpo_id) {//TODO: aplicar sector_id?? porque estamos mostrando de los tres sectores
    //cargamos primero los id de componentes del gpo
    $.ajax({
        context: {gpo_id: gpo_id},
        url: 'https://suri-des.agricultura.gob.mx:8016/api/conceptos/rel/grupos',
        success: function (data) {
            let concs = []
            for (d in data) {
                if (data[d].grupos_concepto_id == this.gpo_id)
                    concs.push(data[d].concepto_id);
            }
            //pedimos conceptos, filtraremos por los ID que traemos
            $.ajax({
                context: {ids: concs},
                url: 'https://suri-des.agricultura.gob.mx:8016/api/conceptos',
                success: function (data) {
                    $("#main-conceptos_list .list-group .concepto-teaser.list-group-item").remove();
                    $("form[name='filtrByProducs'] input[name='productos_filter']").prop('checked',false).change();
                    for (cd in data) {
                        if (this.ids.indexOf(data[cd].concepto_id) != -1) {
                            let cclon = $("#main-conceptos_list .list-group .concepto-teaser.d-none").clone(true);
                            cclon.find('.concepto-title h5').text(data[cd].concepto_titulo)
                            cclon.find('.concepto_principal_beneficio').text(data[cd].concepto_principal_beneficio)
                            cclon.attr('href', cclon.attr('href') + "?cid=" + data[cd].concepto_id)
                            cclon.removeClass('d-none').addClass('list-group-item list-group-item-action');
                            cclon.data('concepto',data[cd]);
                            $("#main-conceptos_list .list-group").append(cclon)
                        }
                    }
                }
            })
        }
    })
}

function initProductFilters() {
    $("#products_filter_container").on('click', '.container-slide .product_switch', function () {
        $(this).find('input').prop('checked', !$(this).find('input').prop('checked'));
        $(this).find('input').change();
    }).on('change', '.container-slide .product_switch input', function () {
        if ($(this).is(':checked')) {
            $(this).parents('.product_switch').addClass('active')
        } else {
            $(this).parents('.product_switch').removeClass('active')
        }
        //update productos_filter
        productos_filter = [];
        $("#product_filter strong").text('');
        $("#products_filter_container .container-slide .product_switch input").each(function () {
            if ($(this).is(':checked')) {
                productos_filter.push($(this).parents('.product_switch').data('producto'));
                let pretxt = $("#product_filter strong").text();
                pretxt += ((pretxt.length > 0) ? ", " : '') + $(this).parents('.product_switch').data('producto').producto;
                $("#product_filter strong").text(pretxt)
            }
        })
        if(productos_filter.length>0) {
            //repasamos todos los conceptos para dejar solo los que coinciden con los filtros de productos
            $("#main-conceptos_list .list-group .concepto-teaser.list-group-item").each(function () {
                $(this).addClass('d-none'); //escondemos siempre
                let this_concepto_id = $(this).data('concepto').concepto_id;
                for (pf in productos_filter) {
                    if (productos_filter[pf].conceptos.indexOf(this_concepto_id) != -1) {
                        $(this).removeClass('d-none');
                    }
                }
            })
        }else{
            $("#main-conceptos_list .list-group .concepto-teaser.list-group-item").removeClass('d-none');
            $("#product_filter strong").text('Todos');
        }
    })
}


function loadProductos(idSec) {
    $.ajax({
        url: 'https://suri-des.agricultura.gob.mx:8016/api/productos',
        success: function (data) {
            for (p in data) {
                if (data[p].id_sector == idSec) {
                    let prodC = $("#products_filter_container .product_switch.d-none").clone(true);
                    prodC.removeClass('d-none');
                    prodC.find('img').attr('src', 'img/alimentos-bg/' + data[p].id + '.jpeg');
                    prodC.find('.layer h6').text(data[p].producto);
                    prodC.find('input[type="checkbox"]').val(data[p].id);
                    prodC.data('producto', data[p]);
                    prodC.data('producto').conceptos=[];//coleccion de conceptos por producto
                    $("#products_filter_container .container-slide").append(prodC);
                }
            }
            //productos cargados, obtenemos los sectores a los que pertenece cada uno
            $.ajax(
                {
                    url: 'https://suri-des.agricultura.gob.mx:8016/api/conceptos/rel/productos',
                    success: function (data) {
                        $("#products_filter_container .product_switch:not(.d-none)").each(function(){
                            //buscamos concepto_id para match con producto
                            for(let rd in data){
                                // console.log(data[rd].sector_producto_id,$(this).data('producto').id);
                                if(data[rd].sector_producto_id == $(this).data('producto').id){
                                    $(this).data('producto').conceptos.push(data[rd].concepto_id);
                                }
                            }
                        })
                    }
                }
            )
        }
    })
}


function initMobileTabs() {
    $(window).scroll(function () {
        if ($("body,html").scrollTop() > $("#main_header").outerHeight() && $(window).width() < 768) {
            $("#side_bar-tipoconcepto").addClass('fixed-tab')
            $("body").css({'margin-top': $("#main_header").outerHeight()})
        } else {
            $("#side_bar-tipoconcepto").removeClass('fixed-tab')
            $("body").css({'margin-top': 'initial'})
        }
    })
}
