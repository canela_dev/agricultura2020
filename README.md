Prototipo presentado para el nuevo concepto del portal de la secretaría, el cual tenía la intención de enfocarse principalmente en el productor, en el beneficiario y en el público en general interesado en obtener información sobre los programas, ya que el sitio oficial en ese momento era más bien una gaceta de eventos sobre los funcionarios y era complicado informarse sobre los programas y el funcionamiento de la secretaría.

El diseño original de la propuesta contemplaba avisos en la pantalla principal, noticias y se planeaban boletines dirigidos a los beneficiarios sobre el inicio de convocatorias, avisos de inicios/fin de temporadas, condiciones ambientales, alertas, etc. El diseño aquí cargado se enfoca en los programas por petición del entonces DG de la secretaría.

Los servicios utilizados en este prototipo es posible que ya no se encuentren activos o que su respuesta ya no sea compatible con la esperada por este demo.

Comparto el diseño pues se trabajó en él y al final la idea original ya no se utilizó, así como faltó definición de los contenidos del home después del bloque de noticias/boletines.
