/**
 * Created by Carlos Canela on 12/02/20.
 */
$(function () {
  $(document).ajaxStart(function () {
    if ($("#main-progress-bar").length == 0) {
      $("body").append('<div id="main-progress-bar" class="progress">\n' +
        '  <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 100%"></div>\n' +
        '</div>')
        $("body").append($('<div>',{id:"loader-layer"}));
    }
  });
  $( document ).ajaxStop(function() {
    $("#main-progress-bar, #loader-layer").remove();
  });
})
